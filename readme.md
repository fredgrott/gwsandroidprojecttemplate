GWSAndroidProjectTemplate
---

# Purpose

Set up new Android Application ANT Build system. Based on the files from the 
Androdi SDk but severely modified to suport dependency management using 
ivy and doclava doc generation and some added features.

# Architecture

## GITIGNORE

Due to Google's choice of using libs with Eclipse ADT to synch the project classpath dependencies 
we need to set the following folders in GITIGNORE to be ignored:

libs
libsjavadocs
antlibs
ext-libs

This will cover all library jar dependencies thus we will be fully under both the Androdi SDK and IVY dependency management controls.

## IVY Settings

In our use case we are putting our libraries specifcially for android app dev in a local repo. Those that require more with 
a remote repo can add that and change ant build scrip tlauch settings to use cache when offline. So we set 
ivysettings file with the base settigns ot access the lcoal repo on the users dev computer.

